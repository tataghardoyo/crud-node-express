const express = require("express")
const morgan = require("morgan")
const bodyParser = express

const connectDB = require("./server/database/connection")

connectDB()

const app = express()

const PORT = 8080

app.use(morgan("tiny"))

app.use(
    bodyParser.urlencoded({
        extended: true
    })
)

app.use(bodyParser.json())

app.use("/", require("./server/routes/router"))

app.listen(PORT,()=>{
    console.log("server is running on http://localhost:"+PORT)
})

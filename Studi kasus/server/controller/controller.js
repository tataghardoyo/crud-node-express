const userdb = require("../model/model")

exports.create = (req, res) =>{
    if(!req.body){
        res.status(400).send({
            message: " data content tidak dapat kosong"
        })
        return;
    }
    const user = new userdb({
        username: req.body.username,
        password: req.body.password,
        name: req.body.name,
        email: req.body.email,
    })

    user.save().then((data) =>{
        res.status(201).json({
            message: "sukses membuat data",
            Data: data,
        })
    })
    .catch((err) =>{
        res.status(500).send({
            message: err.message || "internal server error"
        })
    })
}

exports.find = (req, res) =>{
    if(req.query.username){
        const userName = req.body.username;
        userdb.findOne({
            username: userName,
        })
        .then((data)=>{
            if(!data){
                res.status(404)
                .send({
                    message: "tidak dapat menemukan user "+userName
                })
            }else{
                res.json({
                    message: "sukses dapat data",
                    Data: data,
                })
            }
        })
        .catch((err)=>{
            res.status(500)
            .send({
                message: err.message || "internal server error"
            })
        })
    }else{
        userdb.find()
        .then((data) =>{
            res.json({
                message: "sukses dapatkan semua data",
                Data: data,
            })
        })
        .catch((err) =>{
            res.status(500)
            .send({
                message: err.message || "internal server error"
            })
        })
    }
}

exports.update = (req, res) =>{
    try{
        userdb.findOneAndUpdate({
            username: req.params.username
        },
        req.body, {
            new: true,
        }).then((data)=>{
            if(!data){
                res.status(404).send({
                    message: "tidak dapat update user "+req.params.username,
                })
            }else{
                res.json({
                    message: "sukses update data",
                    Data: data,
                })
            }
        })
    }catch(err){
        res.status(500).send({
            message: err.message || "internal server error"
        })
    }
}

exports.delete = (req,res) =>{
    const userName = req.params.username

    userdb.findOneAndDelete({
        username: userName
    })
    .then((data) =>{
        if(!data){
            res.status(404).send({
                message: "tidak dapat hapus dengan username "+req.params.username,
            })
        }else{
            res.json({
                message: "sukses hapus data"
            })
        }
    })
}
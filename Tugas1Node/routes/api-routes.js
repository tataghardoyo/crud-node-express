const express = require('express')
const router = express.Router()
const contactcontroller = require("../controllers/contactController")
router.get('/', (req, res) => {
    res.json({
        nama: 'Richard Bara',
        TanggalLahir: new Date('January 14, 1994 24:00:00'),
        JenisKelamin: "Laki-Laki",
        Hobi: "Membaca Buku"
    })
})
router.get("/tampil", contactcontroller.index)
router.post("/tambah", contactcontroller.new)
router.get("/tampil/:contact_id", contactcontroller.view)
router.get("/edit/:contact_id", contactcontroller.update)
router.get("/hapus/:contact_id", contactcontroller.delete)

module.exports = router
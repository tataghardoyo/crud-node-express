const express = require('express')
const router = express.Router()
const siswacontroller = require("./siswaController")
router.get('/', (req, res) => {
    res.json({
        nama: 'Richard Bara',
        TanggalLahir: new Date('January 14, 1994 24:00:00'),
        JenisKelamin: "Laki-Laki",
        Hobi: "Membaca Buku"
    })
})
router.get("/tampil", siswacontroller.index)
router.post("/tambah", siswacontroller.new)
router.get("/tampil/:siswa_id", siswacontroller.view)
router.put("/edit/:siswa_id", siswacontroller.update)
router.delete("/hapus/:siswa_id", siswacontroller.delete)

module.exports = router
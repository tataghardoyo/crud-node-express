const Siswa = require("./siswaModel")
exports.index = (req, res) => {
    Siswa.get((err, siswas) => {
        if (err) {
            res.json({
                status: "error",
                message: err,
            })
        }
        res.json({
            status: "sukses",
            message: "data siswa berhasil ditampilkan",
            data: siswas,
        })
    })
}

exports.new = (req, res) => {
    let siswa = new Siswa();
    siswa.nama = req.body.nama ? req.body.nama : siswa.nama;
    siswa.tanggallahir = req.body.tanggallahir;
    siswa.jeniskelamin = req.body.jeniskelamin;
    siswa.hobi = req.body.hobi;
    siswa.save((err) => {
        if (err) {
            res.json({
                status: "error",
                message: err,
            })
        }
        res.json({
            message: "siswa berhasil dibuat",
            data: siswa,
        })
    })
}

exports.view = (req, res) => {
    Siswa.findById(req.params.siswa_id, (err, siswa) => {
        if (err) {
            res.json({
                status: "error",
                message: err,
            })
        }
        res.json({
            message: "menampilkan siswa berdasarkan id",
            data: siswa,
        })
    })
}

exports.update = (req, res) => {
    Siswa.findById(req.params.siswa_id, (err, siswa) => {
        if (err) {
            res.json({
                status: "error",
                message: err,
            })
        }
        siswa.nama = req.body.nama ? req.body.nama : siswa.nama;
        siswa.tanggallahir = req.body.tanggallahir;
        siswa.jeniskelamin = req.body.jeniskelamin;
        siswa.hobi = req.body.hobi;
        siswa.save((err) => {
            if (err) {
                res.json({
                    status: "error",
                    message: err,
                })
            }
            res.json({
                message: "siswa diupdate",
                data: siswa,
            })
        })
    })
}

exports.delete = (req, res) => {
    Siswa.remove({
        _id: req.params.siswa_id
    }, (err, siswa) => {
        if (err)
            res.send(err);

        res.json({
            status: "sukses",
            message: "hapus sukses"
        })
    })
}
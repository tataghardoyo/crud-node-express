const express = require('express')
const router = express.Router()
const mhscontroller = require("./mahasiswaController")

router.get("/mahasiswa", mhscontroller.index)
router.post("/tambah", mhscontroller.new)
router.get("/tampil/:mahasiswa_id", mhscontroller.view)
router.put("/edit/:mahasiswa_id", mhscontroller.update)

module.exports = router
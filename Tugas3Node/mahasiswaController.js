const Mahasiswa = require("./mahasiswaModel")
exports.index = (req, res) => {
    Mahasiswa.get((err, mahasiswas) => {
        if (err) {
            res.json({
                status: "error",
                message: err,
            })
        }
        res.json({
            status: "sukses",
            message: "data mahasiswa berhasil ditampilkan",
            data: mahasiswas,
        })
    })
}

exports.new = (req, res) => {
    let mahasiswa = new Mahasiswa();
    mahasiswa.nim = req.body.nim ? req.body.nim : mahasiswa.nim;
    mahasiswa.nama = req.body.nama;
    mahasiswa.jurusan = req.body.jurusan;
    mahasiswa.semester = req.body.semester;
    mahasiswa.save((err) => {
        if (err) {
            res.json({
                status: "error",
                message: err,
            })
        }
        res.json({
            message: "mahasiswa berhasil dibuat",
            data: mahasiswa,
        })
    })
}

exports.view = (req, res) => {
    Mahasiswa.findById(req.params.mahasiswa_id, (err, mahasiswa) => {
        if (err) {
            res.json({
                status: "error",
                message: err,
            })
        }
        res.json({
            message: "menampilkan mahasiswa berdasarkan id",
            data: mahasiswa,
        })
    })
}

exports.update = (req, res) => {
    Mahasiswa.findById(req.params.mahasiswa_id, (err, mahasiswa) => {
        if (err) {
            res.json({
                status: "error",
                message: err,
            })
        }
        mahasiswa.nim = req.body.nim ? req.body.nim : mahasiswa.nim;
        mahasiswa.nama = req.body.nama;
        mahasiswa.jurusan = req.body.jurusan;
        mahasiswa.semester = req.body.semester;
        mahasiswa.save((err) => {
            if (err) {
                res.json({
                    status: "error",
                    message: err,
                })
            }
            res.json({
                message: "mahasiswa diupdate",
                data: mahasiswa,
            })
        })
    })
}

const express = require('express')
const router = express.Router()
const mhscontroller = require("./mahasiswaController")
router.get('/', (req, res) => {
    res.json({
        nama: 'Richard Bara',
        TanggalLahir: new Date('January 14, 1994 24:00:00'),
        JenisKelamin: "Laki-Laki",
        Hobi: "Membaca Buku"
    })
})
router.get("/mahasiswa", mhscontroller.index)
router.post("/tambah", mhscontroller.new)
router.get("/tampil/:mahasiswa_id", mhscontroller.view)
router.put("/edit/:mahasiswa_id", mhscontroller.update)
router.delete("/hapus/:mahasiswa_id", mhscontroller.delete)

module.exports = router
const express = require('express')
const app = express();
const port = 8081;
const apiRouter = require("./api-routes");
const mongoose = require('mongoose');
const bodyparser = require('express');

app.use(bodyparser.urlencoded({
    extended: true
}))
app.use(bodyparser.json())

mongoose.connect('mongodb://localhost/resthub')
const db = mongoose.connection

app.get("/", (req, res) => {
    res.send("Selamat Datang Di Data Center Mahasiswa")
})
app.use("/api", apiRouter);

app.listen(port, () => {
    console.log(`server berjalan di port ${port}`);
})